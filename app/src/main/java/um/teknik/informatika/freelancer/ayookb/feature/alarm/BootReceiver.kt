package um.teknik.informatika.freelancer.ayookb.feature.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import um.teknik.informatika.freelancer.ayookb.util.NotificationHelper

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        if (intent.action == "android.intent.action.BOOT_COMPLETED") {
            NotificationHelper.scheduleNotification(context)
        }
    }
}
