package um.teknik.informatika.freelancer.ayookb.feature.welcome

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_welcome.*
import um.teknik.informatika.freelancer.ayookb.R
import um.teknik.informatika.freelancer.ayookb.feature.login.LoginActivity

class WelcomeActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, WelcomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        button_next.setOnClickListener {
            startActivity(LoginActivity.getIntent(this))
            overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top)
            finish()
        }
    }
}
