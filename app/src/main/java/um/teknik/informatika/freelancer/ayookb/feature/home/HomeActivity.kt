package um.teknik.informatika.freelancer.ayookb.feature.home

import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bestsoft32.tt_fancy_gif_dialog_lib.TTFancyGifDialog
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_home.*
import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.R
import um.teknik.informatika.freelancer.ayookb.data.PreferenceManager
import um.teknik.informatika.freelancer.ayookb.data.ReminderDatesManager
import um.teknik.informatika.freelancer.ayookb.data.model.User
import um.teknik.informatika.freelancer.ayookb.feature.info.DetailInfoActivity
import um.teknik.informatika.freelancer.ayookb.feature.welcome.WelcomeActivity
import um.teknik.informatika.freelancer.ayookb.util.InfoType

class HomeActivity : AppCompatActivity(), HomeContract.View {

    override lateinit var presenter: HomeContract.Presenter
    private lateinit var timePicker: TimePickerDialog

    companion object {
        fun getIntent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        val datesManager = ReminderDatesManager(this)
        presenter = HomePresenter(this, datesManager)

        timePicker = TimePickerDialog(
            this,
            R.style.CustomTimePickerDialog,
            TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                presenter.setAlarmClock(hour, minute)
            }, 0, 0, false
        )
        button_edit_time_reminder.setOnClickListener {
            timePicker.show()
        }
        button_ganti_profil.setOnClickListener {
            PreferenceManager.resetPreferences(this)
            startActivity(WelcomeActivity.getIntent(this))
            finish()
        }
        with(calendar) {
            addDecorator(
                CalendarKbDecorator(
                    datesManager,
                    ContextCompat.getDrawable(context, R.drawable.bg_highlighted_date)!!
                )
            )
            setOnDateChangedListener { _, calendarDay, _ ->
                presenter.loadSelectedDateInfo(calendarDay.date)
            }
            setSelectedDate(LocalDate.now())
        }
        card_info_kb_umum.setOnClickListener {
            startActivity(DetailInfoActivity.getIntent(this, InfoType.KB_UMUM))
        }
        card_info_kb_pil.setOnClickListener {
            startActivity(DetailInfoActivity.getIntent(this, InfoType.KB_PIL))
        }
        card_info_kb_suntik.setOnClickListener {
            startActivity(DetailInfoActivity.getIntent(this, InfoType.KB_SUNTIK))
        }

        presenter.start()
    }

    override fun onPilReminderActive() {
        text_reminder_msg.setBackgroundResource(R.drawable.bg_bubble_chat_pink)
        text_reminder_msg.setText(R.string.msg_reminder_kb_pil)
        button_reminder_msg.setText(R.string.action_sudah)
        button_reminder_msg.setOnClickListener {
            TTFancyGifDialog.Builder(this)
                .setTitle(getString(R.string.notif_title_kb_pil))
                .setMessage(getString(R.string.notif_content_kb_pil))
                .setGifResource(R.drawable.gif_reminder_dialog)
                .isCancellable(true)
                .setPositiveBtnText("Sudah")
                .setNegativeBtnText("Nanti Saja")
                .setPositiveBtnBackground("#448AFF")
                .setNegativeBtnBackground("#E91E63")
                .OnPositiveClicked {
                    presenter.setTodayReminderStatusCompleted()
                }.OnNegativeClicked {

                }.build()
        }
    }

    override fun onSuntikReminderActive() {
        text_reminder_msg.setBackgroundResource(R.drawable.bg_bubble_chat_pink)
        text_reminder_msg.setText(R.string.msg_reminder_kb_suntik)
        button_reminder_msg.setText(R.string.action_sudah)
        button_reminder_msg.setOnClickListener {
            TTFancyGifDialog.Builder(this)
                .setTitle(getString(R.string.notif_title_kb_pil))
                .setMessage(getString(R.string.notif_content_kb_pil))
                .setGifResource(R.drawable.gif_reminder_dialog)
                .isCancellable(true)
                .setPositiveBtnText("Sudah")
                .setNegativeBtnText("Nanti Saja")
                .setPositiveBtnBackground("#448AFF")
                .setNegativeBtnBackground("#E91E63")
                .OnPositiveClicked {
                    presenter.setTodayReminderStatusCompleted()
                }.OnNegativeClicked {

                }.build()
        }
    }

    override fun onPilReminderCompleted(message: String) {
        if (message.isNotEmpty()) Toasty.success(this, message)
        text_reminder_msg.setBackgroundResource(R.drawable.bg_bubble_chat_blue)
        text_reminder_msg.setText(R.string.msg_kb_pil_finished)
        button_reminder_msg.setText(R.string.action_more)
        button_reminder_msg.setOnClickListener {
            openDetailInfoPage(InfoType.KB_PIL)
        }
    }

    override fun onSuntikReminderCompleted(message: String) {
        if (message.isNotEmpty()) Toasty.success(this, message)
        text_reminder_msg.setBackgroundResource(R.drawable.bg_bubble_chat_blue)
        text_reminder_msg.setText(R.string.msg_kb_suntik_finished)
        button_reminder_msg.setText(R.string.action_more)
        button_reminder_msg.setOnClickListener {
            openDetailInfoPage(InfoType.KB_SUNTIK)
        }
    }

    override fun showUserProfil(user: User, type: String) {
        text_type_reminder.text = type
        text_no_rm.text = user.nomorRM
        text_nama.text = user.namaLengkap
        text_alamat.text = user.alamat
        text_usia.text = user.usia
    }

    override fun showSelectedDateInfo(msgRes: Int) {
        text_calendar_msg.setText(msgRes)
    }

    override fun showAlarmClock(clockString: String) {
        text_time_reminder.text = clockString
    }

    override fun openDetailInfoPage(infoType: InfoType) {
        startActivity(DetailInfoActivity.getIntent(this, infoType))
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

}
