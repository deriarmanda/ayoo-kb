package um.teknik.informatika.freelancer.ayookb.util

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import um.teknik.informatika.freelancer.ayookb.data.PreferenceManager
import um.teknik.informatika.freelancer.ayookb.feature.alarm.AlarmReceiver
import um.teknik.informatika.freelancer.ayookb.feature.alarm.BootReceiver
import java.util.*

object NotificationHelper {

    const val NOTIFICATIONS_CODE = 501
    private var alarmManager: AlarmManager? = null
    private var alarmReceiver: PendingIntent? = null

    fun scheduleNotification(context: Context) {
        val alarmsClock = PreferenceManager.getAlarmsClock(context)
        val calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, alarmsClock.hour)
            set(Calendar.MINUTE, alarmsClock.minute)
        }

        val intent = Intent(context, AlarmReceiver::class.java)
        alarmReceiver = PendingIntent.getBroadcast(
            context,
            NOTIFICATIONS_CODE,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager?.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            alarmReceiver
        )
    }

    fun cancelAlarm() {
        if (alarmManager != null) alarmManager!!.cancel(alarmReceiver)
    }

    fun enableBootReceiver(context: Context) {
        val receiver = ComponentName(context, BootReceiver::class.java)
        context.packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )
    }

    fun disableBootReceiver(context: Context) {
        val receiver = ComponentName(context, BootReceiver::class.java)
        context.packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP
        )
    }
}