package um.teknik.informatika.freelancer.ayookb.data

import android.content.Context
import android.content.SharedPreferences
import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.data.model.AlarmsClock
import um.teknik.informatika.freelancer.ayookb.data.model.User
import um.teknik.informatika.freelancer.ayookb.util.DateUtils

class PreferenceManager {

    companion object {
        private const val PREF_NAME = "ayoo_kb"
        private const val PREF_USER_NOMOR_RM = "user-nomor_rm"
        private const val PREF_USER_NAMA_LENGKAP = "user-nama_lengkap"
        private const val PREF_USER_ALAMAT = "user-alamat"
        private const val PREF_USER_USIA = "user-usia"
        private const val PREF_TGL_AWAL_KUNJUNGAN = "tgl_awal_kunjungan"
        private const val PREF_REMINDER_TYPE = "tipe_penginat"
        private const val PREF_CLOCK_OF_HOUR = "alarm-jam"
        private const val PREF_CLOCK_OF_MINUTE = "alarm-menit"
        private const val PREF_TODAY_REMINDER_STATUS = "today_reminder-status"

        fun isUserAlreadyExists(context: Context): Boolean {
            return getPreference(context).contains(PREF_TGL_AWAL_KUNJUNGAN)
        }

        fun saveUser(context: Context, user: User) {
            val editor = getPreference(context).edit()
            editor.putString(PREF_USER_NOMOR_RM, user.nomorRM)
            editor.putString(PREF_USER_NAMA_LENGKAP, user.namaLengkap)
            editor.putString(PREF_USER_ALAMAT, user.alamat)
            editor.putString(PREF_USER_USIA, user.usia)
            editor.apply()
        }

        fun getUser(context: Context): User {
            val pref = getPreference(context)
            return User(
                pref.getString(PREF_USER_NOMOR_RM, ""),
                pref.getString(PREF_USER_NAMA_LENGKAP, ""),
                pref.getString(PREF_USER_ALAMAT, ""),
                pref.getString(PREF_USER_USIA, "")
            )
        }

        fun saveDate(context: Context, date: LocalDate) {
            val editor = getPreference(context).edit()
            editor.putString(PREF_TGL_AWAL_KUNJUNGAN, DateUtils.formatDate(date))
            editor.apply()
        }

        fun getDate(context: Context): LocalDate {
            val pref = getPreference(context)
            val date = pref.getString(PREF_TGL_AWAL_KUNJUNGAN, "")

            return if (date.isNotEmpty()) DateUtils.parseDate(date) else LocalDate.now()
        }

        fun saveType(context: Context, type: Int) {
            val editor = getPreference(context).edit()
            editor.putInt(PREF_REMINDER_TYPE, type)
            editor.apply()
        }

        fun getType(context: Context): Int {
            val pref = getPreference(context)
            return pref.getInt(PREF_REMINDER_TYPE, ReminderDatesManager.TYPE_NONE)
        }

        fun saveAlarmsClock(context: Context, clock: AlarmsClock) {
            val editor = getPreference(context).edit()
            editor.putInt(PREF_CLOCK_OF_HOUR, clock.hour)
            editor.putInt(PREF_CLOCK_OF_MINUTE, clock.minute)
            editor.apply()
        }

        fun getAlarmsClock(context: Context): AlarmsClock {
            val pref = getPreference(context)
            return AlarmsClock(
                pref.getInt(PREF_CLOCK_OF_HOUR, 8),
                pref.getInt(PREF_CLOCK_OF_MINUTE, 0)
            )
        }

        fun isTodayReminderStatusCompleted(context: Context): Boolean {
            val pref = getPreference(context)
            val dateString = pref.getString(PREF_TODAY_REMINDER_STATUS, "")
            val date =
                if (dateString.isEmpty()) LocalDate.now().minusDays(5)
                else DateUtils.parseDate(dateString)
            return date.isEqual(LocalDate.now())
        }

        fun setTodayReminderStatus(context: Context, date: LocalDate) {
            val editor = getPreference(context).edit()
            editor.putString(PREF_TODAY_REMINDER_STATUS, DateUtils.formatDate(date))
            editor.apply()
        }

        fun resetPreferences(context: Context) {
            val editor = getPreference(context).edit()
            editor.clear()
            editor.apply()
        }

        private fun getPreference(context: Context): SharedPreferences {
            return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        }
    }
}