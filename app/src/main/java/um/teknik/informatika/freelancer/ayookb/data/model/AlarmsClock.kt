package um.teknik.informatika.freelancer.ayookb.data.model

data class AlarmsClock(
    val hour: Int = 8,
    val minute: Int = 0
)