package um.teknik.informatika.freelancer.ayookb.feature.login

import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.base.BasePresenter
import um.teknik.informatika.freelancer.ayookb.base.BaseView

interface LoginContract {

    interface Presenter : BasePresenter {
        fun validateIdentityAndContinue(noKartu: String, nama: String, alamat: String, usia: String)
        fun validateTglAwalAndSave(date: LocalDate, indexType: Int)
    }

    interface View : BaseView<Presenter> {
        fun showIdentityWarningMessage(message: String)
        fun showDateSelectionPage()
        fun onLoginDataSaved()
        fun onLoginDataSaveFailed(message: String)
    }
}