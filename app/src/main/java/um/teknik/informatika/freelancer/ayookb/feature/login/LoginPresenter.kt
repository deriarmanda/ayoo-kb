package um.teknik.informatika.freelancer.ayookb.feature.login

import android.content.Context
import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.data.PreferenceManager
import um.teknik.informatika.freelancer.ayookb.data.ReminderDatesManager
import um.teknik.informatika.freelancer.ayookb.data.model.User
import um.teknik.informatika.freelancer.ayookb.util.NotificationHelper

class LoginPresenter(
    private val view: LoginContract.View
) : LoginContract.Presenter {

    private val context = view as Context

    override fun validateIdentityAndContinue(noKartu: String, nama: String, alamat: String, usia: String) {
        if (noKartu.isNotBlank() && nama.isNotBlank()
            && alamat.isNotBlank() && usia.isNotBlank()
        ) {
            PreferenceManager.saveUser(
                context,
                User(
                    noKartu,
                    nama,
                    alamat,
                    "$usia Tahun"
                )
            )
            view.showDateSelectionPage()
        } else {
            view.showIdentityWarningMessage("Field tidak boleh ada yang kosong !")
        }
    }

    override fun validateTglAwalAndSave(date: LocalDate, indexType: Int) {
        val type =
            if (indexType == 0) ReminderDatesManager.TYPE_PIL
            else if (indexType == 1) ReminderDatesManager.TYPE_SUNTIK
            else ReminderDatesManager.TYPE_NONE
        PreferenceManager.saveType(context, type)
        PreferenceManager.saveDate(context, date)

        NotificationHelper.scheduleNotification(context)
        NotificationHelper.enableBootReceiver(context)
        view.onLoginDataSaved()
    }

    override fun start() {}
}