package um.teknik.informatika.freelancer.ayookb.feature.home

import android.graphics.drawable.Drawable
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import um.teknik.informatika.freelancer.ayookb.data.ReminderDatesManager

class CalendarKbDecorator(
    private val datesManager: ReminderDatesManager,
    private val bgCircle: Drawable
) : DayViewDecorator {

    override fun shouldDecorate(day: CalendarDay?): Boolean {
        return if (day != null) {
            datesManager.isDateShouldBeHighlighted(day.date)
        } else false
    }

    override fun decorate(day: DayViewFacade?) {
        //day?.addSpan(DotSpan(8f, Color.parseColor("#E91E63")))
        day?.setBackgroundDrawable(bgCircle)
    }
}