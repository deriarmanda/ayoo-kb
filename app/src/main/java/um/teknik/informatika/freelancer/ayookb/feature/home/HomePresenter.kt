package um.teknik.informatika.freelancer.ayookb.feature.home

import android.content.Context
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import um.teknik.informatika.freelancer.ayookb.R
import um.teknik.informatika.freelancer.ayookb.data.PreferenceManager
import um.teknik.informatika.freelancer.ayookb.data.ReminderDatesManager
import um.teknik.informatika.freelancer.ayookb.data.model.AlarmsClock
import um.teknik.informatika.freelancer.ayookb.util.NotificationHelper

class HomePresenter(
    private val view: HomeContract.View,
    private val datesManager: ReminderDatesManager
) : HomeContract.Presenter {

    private val context = view as Context

    override fun checkTodayReminderStatus() {
        val isComplete = PreferenceManager.isTodayReminderStatusCompleted(context)
        val isSuntikReminder = datesManager.isDateShouldBeHighlighted(LocalDate.now())
        val alarmClock = PreferenceManager.getAlarmsClock(context)
        val reminderTime = LocalDateTime.now()
            .withHour(alarmClock.hour)
            .withMinute(alarmClock.minute)
        val currentTime = LocalDateTime.now()
        val type = PreferenceManager.getType(context)

        if (!isComplete) {
            if (currentTime.isAfter(reminderTime)) {
                if (isSuntikReminder) view.onSuntikReminderActive()
                else {
                    when (type) {
                        ReminderDatesManager.TYPE_SUNTIK -> view.onSuntikReminderCompleted("")
                        ReminderDatesManager.TYPE_PIL -> view.onPilReminderActive()
                        else -> view.onPilReminderCompleted("")
                    }
                }
            }
        } else {
            if (isSuntikReminder) view.onSuntikReminderCompleted("")
            else {
                if (type == ReminderDatesManager.TYPE_SUNTIK) view.onSuntikReminderCompleted("")
                else view.onPilReminderCompleted("")
            }
        }
    }

    override fun setTodayReminderStatusCompleted() {
        val now = LocalDate.now()
        PreferenceManager.setTodayReminderStatus(context, now)
        //val isSuntikReminder = datesManager.isDateShouldBeHighlighted(now)
        val isSuntikReminder = PreferenceManager.getType(context) == ReminderDatesManager.TYPE_SUNTIK
        if (isSuntikReminder) view.onSuntikReminderCompleted(context.getString(R.string.msg_kb_suntik_finished_short))
        else view.onPilReminderCompleted(context.getString(R.string.msg_kb_pil_finished_short))
    }

    override fun loadUserProfile() {
        val user = PreferenceManager.getUser(context)
        val type = PreferenceManager.getType(context)
        val textType =
            if (type == ReminderDatesManager.TYPE_SUNTIK) "Jenis Pengingat Suntik KB"
            else "Jenis Pengingat Pil KB"
        view.showUserProfil(user, textType)
    }

    override fun loadSelectedDateInfo(date: LocalDate) {
        if (PreferenceManager.getType(context) == ReminderDatesManager.TYPE_SUNTIK) {
            if (datesManager.isDateShouldBeHighlighted(date)) {
                view.showSelectedDateInfo(R.string.msg_calendar_desc_kb_suntik)
            } else {
                view.showSelectedDateInfo(R.string.msg_calendar_desc_kb_suntik_empty)
            }
        } else {
            view.showSelectedDateInfo(R.string.msg_calendar_desc_kb_pil)
        }
    }

    override fun loadAlarmClock() {
        val alarmsClock = PreferenceManager.getAlarmsClock(context)
        view.showAlarmClock(
            String.format(
                "%s:%s",
                if (alarmsClock.hour < 10) "0${alarmsClock.hour}" else alarmsClock.hour,
                if (alarmsClock.minute < 10) "0${alarmsClock.minute}" else alarmsClock.minute
            )
        )
    }

    override fun setAlarmClock(hour: Int, minute: Int) {
        val alarmsClock = AlarmsClock(hour, minute)
        PreferenceManager.saveAlarmsClock(context, alarmsClock)

        NotificationHelper.cancelAlarm()
        NotificationHelper.disableBootReceiver(context)
        NotificationHelper.scheduleNotification(context)
        NotificationHelper.enableBootReceiver(context)

        view.showAlarmClock(
            String.format(
                "%s:%s",
                if (alarmsClock.hour < 10) "0${alarmsClock.hour}" else alarmsClock.hour,
                if (alarmsClock.minute < 10) "0${alarmsClock.minute}" else alarmsClock.minute
            )
        )
    }

    override fun start() {
        checkTodayReminderStatus()
        loadUserProfile()
        loadSelectedDateInfo(LocalDate.now())
        loadAlarmClock()
    }
}