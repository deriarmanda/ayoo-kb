package um.teknik.informatika.freelancer.ayookb.feature.home

import android.support.annotation.StringRes
import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.base.BasePresenter
import um.teknik.informatika.freelancer.ayookb.base.BaseView
import um.teknik.informatika.freelancer.ayookb.data.model.User
import um.teknik.informatika.freelancer.ayookb.util.InfoType

interface HomeContract {

    interface Presenter : BasePresenter {
        fun checkTodayReminderStatus()
        fun setTodayReminderStatusCompleted()
        fun loadUserProfile()
        fun loadSelectedDateInfo(date: LocalDate)
        fun loadAlarmClock()
        fun setAlarmClock(hour: Int, minute: Int)
    }

    interface View : BaseView<Presenter> {
        fun onPilReminderActive()
        fun onSuntikReminderActive()
        fun onPilReminderCompleted(message: String)
        fun onSuntikReminderCompleted(message: String)
        fun showUserProfil(user: User, type: String)
        fun showSelectedDateInfo(@StringRes msgRes: Int)
        fun showAlarmClock(clockString: String)
        fun openDetailInfoPage(infoType: InfoType)
    }
}