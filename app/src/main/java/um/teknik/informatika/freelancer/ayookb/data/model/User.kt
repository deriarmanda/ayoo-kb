package um.teknik.informatika.freelancer.ayookb.data.model

data class User(
    val nomorRM: String,
    val namaLengkap: String,
    val alamat: String,
    val usia: String
)