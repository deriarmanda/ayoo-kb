package um.teknik.informatika.freelancer.ayookb.base

interface BaseCallback<T> {
    fun onSuccess(t: T)
    fun onFailed(message: String)
}