package um.teknik.informatika.freelancer.ayookb.data

import android.content.Context
import android.util.Log
import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.util.DateUtils

class ReminderDatesManager(private val context: Context) {

    private var firstDate: LocalDate = LocalDate.now()
    private val dateList = arrayListOf<LocalDate>()

    companion object {
        const val TYPE_NONE = 0
        const val TYPE_PIL = 1
        const val TYPE_SUNTIK = 2
    }

    fun getFirstDate(): LocalDate {
        loadFirstDate()
        return firstDate
    }

    fun getHighlightedDates(): List<LocalDate> {
        if (dateList.isEmpty()) loadHighlightedDates()
        return dateList
    }

    fun isDateShouldBeHighlighted(date: LocalDate): Boolean {
        if (PreferenceManager.getType(context) != TYPE_SUNTIK) return false
        if (dateList.isEmpty()) loadHighlightedDates()
        var flag = false
        for (highlightedDate in dateList) {
            if (highlightedDate.isEqual(date)) {
                flag = true
                break
            }
        }

        return flag
    }

    private fun loadFirstDate() {
        firstDate = PreferenceManager.getDate(context)
    }

    private fun loadHighlightedDates() {
        loadFirstDate()
        var multiplier: Long = 28
        for (count in 1..120) {
            val nextDate = firstDate.plusDays(multiplier)
            dateList.add(nextDate)
            multiplier += 28

            Log.d("ReminderDatesManager", "Next Highlighted Date: ${DateUtils.formatDate(nextDate)}")
        }
    }
}