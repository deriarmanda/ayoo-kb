package um.teknik.informatika.freelancer.ayookb.base

interface BasePresenter {
    fun start()
}