package um.teknik.informatika.freelancer.ayookb.util

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.annotation.StyleRes
import um.teknik.informatika.freelancer.ayookb.R

enum class InfoType(
    @StyleRes val style: Int,
    @StringRes val title: Int,
    @StringRes val content: Int,
    @DrawableRes val image: Int
) {
    KB_UMUM(
        R.style.AppTheme_InfoKbUmum,
        R.string.msg_title_kb_umum,
        R.string.text_info_kb_umum,
        R.drawable.img_ayo_ikut_kb
    ),
    KB_PIL(
        R.style.AppTheme_InfoKbPil,
        R.string.msg_title_kb_pil,
        R.string.text_info_kb_pil,
        R.drawable.img_pil_kb
    ),
    KB_SUNTIK(
        R.style.AppTheme_InfoKbSuntik,
        R.string.msg_title_kb_suntik,
        R.string.text_info_kb_suntik,
        R.drawable.img_suntik_kb
    )
}