package um.teknik.informatika.freelancer.ayookb.base

interface BaseView<T> {
    val presenter: T
    fun setLoadingIndicator(active: Boolean)
}