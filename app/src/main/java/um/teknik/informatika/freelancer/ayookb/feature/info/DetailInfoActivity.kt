package um.teknik.informatika.freelancer.ayookb.feature.info

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail_info.*
import um.teknik.informatika.freelancer.ayookb.R
import um.teknik.informatika.freelancer.ayookb.util.InfoType

class DetailInfoActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_INFO = "extra_detail_informasi"
        fun getIntent(context: Context, infoType: InfoType): Intent {
            val intent = Intent(context, DetailInfoActivity::class.java)
            intent.putExtra(EXTRA_INFO, infoType)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val infoType = intent.getSerializableExtra(EXTRA_INFO) as InfoType
        setTheme(infoType.style)
        setContentView(R.layout.activity_detail_info)
        supportActionBar?.setTitle(infoType.title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        text_desc.setText(infoType.content)
        image_desc.setImageResource(infoType.image)
    }

}
