package um.teknik.informatika.freelancer.ayookb.util

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

object DateUtils {

    private val COMPLETE_FORMATTER = DateTimeFormatter.ofPattern("EEEE, d MMMM yyyy")
    private val DATE_FORMATTER = DateTimeFormatter.ofPattern("d MMMM yyyy")

    fun formatCompleteDate(date: LocalDate) = date.format(COMPLETE_FORMATTER) ?: "null"
    fun formatDate(date: LocalDate) = date.format(DATE_FORMATTER) ?: "null"
    fun parseDate(dateString: String) = LocalDate.parse(dateString, DATE_FORMATTER)
}