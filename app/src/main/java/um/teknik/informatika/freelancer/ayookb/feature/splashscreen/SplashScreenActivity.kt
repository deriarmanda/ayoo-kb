package um.teknik.informatika.freelancer.ayookb.feature.splashscreen

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import um.teknik.informatika.freelancer.ayookb.data.PreferenceManager
import um.teknik.informatika.freelancer.ayookb.feature.home.HomeActivity
import um.teknik.informatika.freelancer.ayookb.feature.welcome.WelcomeActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val timer = object : CountDownTimer(1000, 1000) {
            override fun onTick(p0: Long) {}
            override fun onFinish() {
                val context = this@SplashScreenActivity
                if (PreferenceManager.isUserAlreadyExists(context)) startActivity(HomeActivity.getIntent(context))
                else startActivity(WelcomeActivity.getIntent(context))
                finish()
            }
        }
        timer.start()
    }
}
