package um.teknik.informatika.freelancer.ayookb.feature.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_login.*
import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.R
import um.teknik.informatika.freelancer.ayookb.feature.home.HomeActivity
import um.teknik.informatika.freelancer.ayookb.util.DateUtils

class LoginActivity : AppCompatActivity(), LoginContract.View {

    override lateinit var presenter: LoginContract.Presenter

    companion object {
        fun getIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(this)

        with(view_flipper) {
            setFlipInterval(1000)
            isAutoStart = false
            setInAnimation(
                this@LoginActivity,
                R.anim.in_from_right
            )
            setOutAnimation(
                this@LoginActivity,
                R.anim.out_to_left
            )
        }
        with(calendar) {
            setSelectedDate(LocalDate.now())
            setOnDateChangedListener { _, calendarDay, _ ->
                val date = DateUtils.formatCompleteDate(calendarDay.date)
                text_date.text = date
            }
        }
        button_lanjutkan.setOnClickListener {
            presenter.validateIdentityAndContinue(
                form_no_kartu.text.toString(),
                form_nama.text.toString(),
                form_alamat.text.toString(),
                form_usia.text.toString()
            )
        }
        button_simpan.setOnClickListener {
            presenter.validateTglAwalAndSave(
                calendar.selectedDate?.date!!,
                spinner_type.selectedItemPosition
            )
        }
        text_date.text = DateUtils.formatCompleteDate(LocalDate.now())
    }

    override fun showIdentityWarningMessage(message: String) {
        Toasty.warning(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showDateSelectionPage() {
        view_flipper.showNext()
    }

    override fun onLoginDataSaved() {
        startActivity(HomeActivity.getIntent(this))
        overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top)
        finish()
    }

    override fun onLoginDataSaveFailed(message: String) {
        Toasty.warning(this, message, Toast.LENGTH_LONG).show()
    }

    override fun setLoadingIndicator(active: Boolean) {}

}
