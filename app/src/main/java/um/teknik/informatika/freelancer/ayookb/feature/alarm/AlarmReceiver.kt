package um.teknik.informatika.freelancer.ayookb.feature.alarm

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import org.threeten.bp.LocalDate
import um.teknik.informatika.freelancer.ayookb.R
import um.teknik.informatika.freelancer.ayookb.data.PreferenceManager
import um.teknik.informatika.freelancer.ayookb.data.ReminderDatesManager
import um.teknik.informatika.freelancer.ayookb.feature.splashscreen.SplashScreenActivity
import um.teknik.informatika.freelancer.ayookb.util.NotificationHelper

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        val type = PreferenceManager.getType(context)
        val datesManager = ReminderDatesManager(context)
        val isSuntikReminder = datesManager.isDateShouldBeHighlighted(LocalDate.now())
        if (!PreferenceManager.isUserAlreadyExists(context)) return
        if (type == ReminderDatesManager.TYPE_NONE) return
        if (type == ReminderDatesManager.TYPE_SUNTIK && !isSuntikReminder) return

        val targetIntent = Intent(context, SplashScreenActivity::class.java)
        targetIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

        val pendingIntent = PendingIntent.getActivity(
            context,
            NotificationHelper.NOTIFICATIONS_CODE,
            targetIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val title =
            if (isSuntikReminder) context.getString(R.string.notif_title_kb_suntik)
            else context.getString(R.string.notif_title_kb_pil)
        val notification = buildNotification(context, pendingIntent, title)

        val notifManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notifManager.notify(NotificationHelper.NOTIFICATIONS_CODE, notification)
    }

    private fun buildNotification(context: Context, pendingIntent: PendingIntent, title: String): Notification {
        /*val content =
            if (isSuntikReminder) context.getString(R.string.notif_content_kb_suntik)
            else context.getString(R.string.notif_content_kb_pil)*/

        val notificationBuilder = NotificationCompat.Builder(
            context,
            "ayoo_kb-notification"
        ).setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_notification_small_48dp)
            .setColor(Color.parseColor("#FFA000"))
            .setColorized(true)
            .setContentTitle("Pengingat dari Ayoo KB")
            .setContentText(title)
            .setAutoCancel(true)
            .setOngoing(true)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setPriority(NotificationCompat.PRIORITY_MAX)
        return notificationBuilder.build()
    }
}
